package com.example.VerwaltungsBuchungsSystemVersion2.controllers;

import com.example.VerwaltungsBuchungsSystemVersion2.dtos.*;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost.KompletteBuchungDTOPost;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.Buchung;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.BuchungsPerson;
import com.example.VerwaltungsBuchungsSystemVersion2.services.BuchungsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class BuchungsUserController {

    @Autowired
    BuchungsUserService buchungsUserService;

    @CrossOrigin
    @GetMapping("/buchungsperson/{buchungspersonId}")
    public BuchungsPerson getBuchungsPerson(@PathVariable int buchungspersonId) {
        return buchungsUserService.getBuchungsPerson(buchungspersonId);
    }

    @CrossOrigin
    @DeleteMapping(value = "/buchung/{buchungId}")
    public void deleteBuchung(@PathVariable int buchungId) {
        buchungsUserService.delete(buchungId);
    }

    @CrossOrigin
    @GetMapping("/buchung/{buchungId}")
    public Buchung getBuchung(@PathVariable int buchungId) {
        return buchungsUserService.getBuchung(buchungId);
    }

    @CrossOrigin
    @GetMapping("/zimmerverfuegbarkeit")
    public Map<String, Integer> getAufgelisteteFreieZimmer(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date von, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date bis) {
        ZeitraumDTO zeitraum = new ZeitraumDTO(von, bis);
        return buchungsUserService.getAufgelisteteFreieZimmer(zeitraum);
    }

    @CrossOrigin
    @GetMapping("/buchungsuebersicht/{buchungId}")
    public BuchungsUeberblickDTO getBuchungsUeberlich(@PathVariable int buchungId) {
        return buchungsUserService.getbuchungsUeberblickDTO(buchungId);
    }

    @CrossOrigin
    @GetMapping("/buchungsuebersicht/preisuebersicht/{hotelId}/{buchungId}")
    public PreisBerechnungDTO getpreisBerechnung(@PathVariable int hotelId, @PathVariable int buchungId) {
        return buchungsUserService.preisBerechnung(buchungId, hotelId);
    }

    @CrossOrigin
    @GetMapping("/buchungsuebersicht/stornoberechnung/{hotelId}/{buchungId}")
    public StornoDTO getStornoBerechnung(@PathVariable int hotelId, @PathVariable int buchungId) {
        return buchungsUserService.stornoBerechnung(buchungId, hotelId);
    }

    @CrossOrigin
    @PostMapping(value = "/buchungsperson", consumes = "application/json")
    public void postBuchungsPerson(@RequestBody BuchungsPerson buchungsPerson) {
        buchungsUserService.postBuchungsPerson(buchungsPerson);
    }

    @CrossOrigin
    @GetMapping("/buchungsueberesicht/dto/buchung/{buchungsPersonId}")
    public BuchungsDTO getBuchungsBerechnung(@PathVariable int buchungsPersonId) {
        return buchungsUserService.buchungsBerechnung(buchungsPersonId);
    }

    @CrossOrigin
    @PostMapping(value = "/buchung", consumes = "application/json")
    public void postBuchung(@RequestBody KompletteBuchungDTOPost kompletteBuchungDTOPost) {
        buchungsUserService.postBuchung(kompletteBuchungDTOPost);
    }

    @CrossOrigin
    @GetMapping("/buchung/buchungsuebersicht/{buchungsPersonId}")
    public KompletteBuchungDTOPost getKompletteBuchungUebersicht(@PathVariable int buchungsPersonId) {
        return buchungsUserService.kompletteBuchungUebersicht(buchungsPersonId);
    }
}

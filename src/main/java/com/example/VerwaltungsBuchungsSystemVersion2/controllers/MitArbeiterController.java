package com.example.VerwaltungsBuchungsSystemVersion2.controllers;

import com.example.VerwaltungsBuchungsSystemVersion2.dtos.RezeptionDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.ZeitraumDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.Buchung;
import com.example.VerwaltungsBuchungsSystemVersion2.services.MitArbeiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/mitarbeiter/")
public class MitArbeiterController {

    @Autowired
    MitArbeiterService mitArbeiterService;

    @CrossOrigin
    @PostMapping(value = "/buchung", consumes = "application/json")
    public void postBuchung(@RequestBody Buchung buchung) {
        mitArbeiterService.post(buchung);
    }

    @CrossOrigin
    @DeleteMapping("/buchung/{buchungsId}")
    public void deleteBuchung(@PathVariable int buchungId) {
        mitArbeiterService.deleteBuchung(buchungId);
    }

    @CrossOrigin
    @GetMapping("/rezeption")
    public RezeptionDTO getAlleBuchungen(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date von, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date bis) {
        ZeitraumDTO zeitraum = new ZeitraumDTO(von, bis);
        return mitArbeiterService.alleBuchungen(zeitraum);
    }
}

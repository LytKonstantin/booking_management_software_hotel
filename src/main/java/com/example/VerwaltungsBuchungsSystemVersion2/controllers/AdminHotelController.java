package com.example.VerwaltungsBuchungsSystemVersion2.controllers;

import com.example.VerwaltungsBuchungsSystemVersion2.dtos.BilanzDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.InfoAbteilungDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.*;
import com.example.VerwaltungsBuchungsSystemVersion2.services.AdminHotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * The Admin-Controller communicate with the Front-End. Only the Hotel-Staff have Access to that.
 */
@RestController
@RequestMapping("/admin")
public class AdminHotelController {

    @Autowired
    AdminHotelService adminHotelService;

    /**
     * Deleting process of an Employee.
     *
     * @param mitArbeiterId You need the IEmployeeID, for the deleting-process.
     */
    @CrossOrigin
    @DeleteMapping("/mitarbeiter/{mitArbeiterId}")
    public void deleteMitarbeiter(@PathVariable int mitArbeiterId) {
        adminHotelService.deleteMitArbeiter(mitArbeiterId);
    }

    /**
     * Deleting process of a Room.
     *
     * @param zimmerId You need the RoomID, for the deleting-process.
     */
    @CrossOrigin
    @DeleteMapping("/zimmer/{zimmerId}")
    public void deleteZimmer(@PathVariable int zimmerId) {
        adminHotelService.deleteZimmer(zimmerId);
    }

    /**
     * First version of deleting without of the calculation cancellation progress
     *
     * @param buchungId You need the BookingID, for delete.
     */
    @CrossOrigin
    @DeleteMapping("/buchung/{buchungId}")
    public void deleteBuchung(@PathVariable int buchungId) {
        adminHotelService.deleteBuchung(buchungId);
    }

    /**
     * Get process of all Employees.
     *
     * @return List of all Employees.
     */
    @CrossOrigin
    @GetMapping("/mitarbeiter")
    public List getALLMitarbeiter() {
        return adminHotelService.getALLMitarbeiter();
    }

    /**
     * @param buchungsPersonId BookingPersonId to see the information of the Person.
     * @return Information of the BookingPerson
     */
    @CrossOrigin
    @GetMapping("/buchungsperson/{buchungsPersonId}")
    public BuchungsPerson getBuchungsperson(@PathVariable int buchungsPersonId) {
        return adminHotelService.getBuchungsPerson(buchungsPersonId);
    }

    /**
     * @param buchungsPerson   You send The Information that you want to change of the BookingPerson.
     * @param buchungsPersonId The Id of the Person you want to change.
     */
    @CrossOrigin
    @PutMapping(value = "/buchungsperson/{buchungsPersonId}", consumes = "application/json")
    public void putBuchungsperson(@RequestBody BuchungsPerson buchungsPerson, @PathVariable int buchungsPersonId) {
        adminHotelService.put(buchungsPerson, buchungsPersonId);
    }

    /**
     * @param mitArbeiterId EmployeeID to that one you want to see.
     * @return All Information of the Employee.
     */
    @CrossOrigin
    @GetMapping("/mitarbeiter/{mitArbeiterId}")
    public MitArbeiter getMitarbeiter(@PathVariable int mitArbeiterId) {
        return adminHotelService.getMitArbeiter(mitArbeiterId);
    }

    /**
     * @param abteilung Enum of the Department.
     * @return A List of All Employees that work in this Department.
     */
    @CrossOrigin
    @GetMapping("/mitarbeiter/abteilung/{abteilung}")
    public List getALLMitarbeiterPerAbteilung(@PathVariable String abteilung) {
        return adminHotelService.getALLMitarbeiterPerAbteilung(abteilung);
    }

    /**
     * @param geschlecht Enum of the Gender
     * @return A List of all Employees with the chosen Gender.
     */
    @CrossOrigin
    @GetMapping("/mitarbeiter/gender({geschlecht}")
    public List getALLMitarbeiterPerGeschlecht(@PathVariable String geschlecht) {
        return adminHotelService.getALLMitarbeiterPerGeschlecht(geschlecht);
    }

    /**
     * @param zimmerId RoomID
     * @return All Information of the Room.
     */
    @CrossOrigin
    @GetMapping("/zimmer/{zimmerId}")
    public Zimmer getZimmer(@PathVariable int zimmerId) {
        return adminHotelService.getZimmer(zimmerId);
    }

    /**
     * @param buchungId BookingID
     * @return Information of the Booking.
     */
    @CrossOrigin
    @GetMapping("/buchung/{buchungId}")
    public Buchung getBuchung(@PathVariable int buchungId) {
        return adminHotelService.getBuchung(buchungId);
    }

    /**
     * Just a Count-Function of All Employees.
     *
     * @return Integer of all Employees.
     */
    @CrossOrigin
    @GetMapping("/mitarbeiter/info")
    public int getMitarbeiterAnzahl() {
        return adminHotelService.getMitarbeiterAnzahl();
    }

    /**
     * Information how many Departments we have and have many Employees work for them.
     *
     * @return Set of Departments and Employees.
     */
    @CrossOrigin
    @GetMapping("/mitarbeiter/info/abteilung")
    public Set<InfoAbteilungDTO> getInfoAbteilung() {
        return adminHotelService.getInfoAbteilung();
    }

    /**
     * @param mitArbeiter We get The necessary Information of an Employee that start to work for the Hotel.
     */
    @CrossOrigin
    @PostMapping(value = "/mitarbeiter", consumes = "application/json")
    public void postMitarbeiter(@RequestBody MitArbeiter mitArbeiter) {
        adminHotelService.post(mitArbeiter);
    }

    /**
     * @param zimmer Create with the Information a New Room in the Hotel.
     */
    @CrossOrigin
    @PostMapping(value = "/zimmer", consumes = "application/json")
    public void postZimmer(@RequestBody Zimmer zimmer) {
        adminHotelService.post(zimmer);
    }

    /**
     * Change the Information of the Hotel.
     *
     * @param hotel All the Information we want to change.
     */
    @CrossOrigin
    @PostMapping(value = "/hotel", consumes = "application/json")
    public void postHotel(@RequestBody Hotel hotel) {
        adminHotelService.post(hotel);
    }

    /**
     * @param zimmer   Information that we want to change of us Room.
     * @param zimmerId Find the Room with the RoomId.
     */
    @CrossOrigin
    @PutMapping(value = "/zimmer/{zimmerId}", consumes = "application/json")
    public void putZimmer(@RequestBody Zimmer zimmer, @PathVariable int zimmerId) {
        adminHotelService.put(zimmer, zimmerId);
    }

    /**
     * Change the information of a Co-Employee.
     *
     * @param mitArbeiter   The Information that We want to change.
     * @param mitArbeiterId The EmployeeId that we can Find the right person.
     */
    @CrossOrigin
    @PutMapping(value = "/miarbeiter/{mitarbeiterId}", consumes = "application/json")
    public void putMitarbeiter(@RequestBody MitArbeiter mitArbeiter, @PathVariable int mitArbeiterId) {
        adminHotelService.put(mitArbeiter, mitArbeiterId);
    }

    /**
     * Not really necessary in my concept.(If you already got a Booking, you need to cancelled it first and then booking again.)
     *
     * @param buchung   Things you want to change.
     * @param buchungId BookingId that you can find the Booking.
     */
    @CrossOrigin
    @PutMapping(value = "/buchung/{buchungId}", consumes = "application/json")
    public void putBuchung(@RequestBody Buchung buchung, @PathVariable int buchungId) {
        adminHotelService.put(buchung, buchungId);
    }

    /**
     * You get a Balance Sheet of the Hotel
     *
     * @param hotelId The HotelId in order to find the Hotel
     * @return The Balance Sheet of the Hotel.
     */
    @CrossOrigin
    @GetMapping("/bilanz/{hotelId}")
    public BilanzDTO getBilanz(@PathVariable int hotelId) {
        return adminHotelService.bilanzRechnung(hotelId);
    }

    /**
     * Create a new Booking Person.
     *
     * @param buchungsPerson The needed Information to create a Booking Person.
     */
    @CrossOrigin
    @PostMapping(value = "/buchungsPerson", consumes = "application/json")
    public void postBuchungsPerson(@RequestBody BuchungsPerson buchungsPerson) {
        adminHotelService.post(buchungsPerson);
    }

}

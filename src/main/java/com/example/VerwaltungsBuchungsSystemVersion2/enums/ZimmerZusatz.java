package com.example.VerwaltungsBuchungsSystemVersion2.enums;

public enum ZimmerZusatz {
    BALKON("Balkon"),
    GARTEN("Garten"),
    MEERBLICK("Meerblick");

    private final String name;

    ZimmerZusatz(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ZimmerZusatz convertZimmerZusatz(String zimmmerZusatz) {
        switch (zimmmerZusatz) {
            case "Balkon":
                return ZimmerZusatz.BALKON;
            case "Garten":
                return ZimmerZusatz.GARTEN;
            case "Meerblick":
                return ZimmerZusatz.MEERBLICK;
            default:
                return null;
        }
    }
}

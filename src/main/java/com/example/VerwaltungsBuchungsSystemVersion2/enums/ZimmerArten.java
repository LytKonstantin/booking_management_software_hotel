package com.example.VerwaltungsBuchungsSystemVersion2.enums;

public enum ZimmerArten {
    DOPPELZIMMER(200, 2, "Doppel Zimmer"),
    EINZELZIMMER(100, 1, "Einzel Zimmer"),
    JUNIORSUITE(300, 4, "Junior Suite"),
    SENIORSUITE(400, 6, "Senior Suite");

    private final int preisProZimmer;
    private final int schlafPlaetze;
    private final String name;

    ZimmerArten(int preisProZimmer, int schlafPlaetze, String name) {
        this.preisProZimmer = preisProZimmer;
        this.schlafPlaetze = schlafPlaetze;
        this.name = name;
    }

    public int getPreisProZimmer() {
        return preisProZimmer;
    }

    public int getSchlafPlaetze() {
        return schlafPlaetze;
    }

    public String getName() {
        return name;
    }

    public ZimmerArten convertZimmerArten(String zimmmerArten) {
        switch (zimmmerArten) {
            case "Doppel Zimmer":
                return ZimmerArten.DOPPELZIMMER;
            case "Einzel Zimmer":
                return ZimmerArten.EINZELZIMMER;
            case "Junior Suite":
                return ZimmerArten.JUNIORSUITE;
            case "Senior Suite":
                return ZimmerArten.SENIORSUITE;
            default:
                return null;
        }
    }
}

package com.example.VerwaltungsBuchungsSystemVersion2.enums;

public enum Verpflegung {

    ALLINCLUSIVE(8, "All Inklusive"),
    FRUEHSTUECK(3, "Frühstück"),
    HALBPENSION(5, "Halb Pension");

    private final int preisDerVerplegung;
    private final String name;


    Verpflegung(int preisDerVerplegung, String name) {
        this.preisDerVerplegung = preisDerVerplegung;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPreisDerVerplegung() {
        return preisDerVerplegung;
    }

    public Verpflegung convertVerpflegung(String verpflegung) {
        switch (verpflegung) {
            case "All Inklusive":
                return Verpflegung.ALLINCLUSIVE;
            case "Frühstück":
                return Verpflegung.FRUEHSTUECK;
            case "Halb Pension":
                return Verpflegung.HALBPENSION;

            default:
                return null;
        }
    }
}

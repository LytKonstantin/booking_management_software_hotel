package com.example.VerwaltungsBuchungsSystemVersion2.enums;

public enum Extras {
    HOCHZEITSREISE("Hochzeits Reise"),
    GEBURTSTAG("Geburtstag"),
    AIRPORTSERVICE("Airport Service"),
    SELBSTANREISE("Selbstanreise"),
    NONE("");

    private final String name;

    Extras(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Extras convertExtras(String extra) {
        switch (extra) {
            case "Hochzeits Reise":
                return Extras.HOCHZEITSREISE;
            case "Geburtstag":
                return Extras.GEBURTSTAG;
            case "Airport Service":
                return Extras.AIRPORTSERVICE;
            case "Selbstanreise":
                return Extras.SELBSTANREISE;
            default:
                return Extras.NONE;
        }
    }
}

package com.example.VerwaltungsBuchungsSystemVersion2;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.*;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.*;
import com.example.VerwaltungsBuchungsSystemVersion2.repositories.*;
import com.example.VerwaltungsBuchungsSystemVersion2.services.BuchungsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class VerwaltungsBuchungsSystemVersion2Application implements CommandLineRunner {

    @Autowired
    BuchungsPersonCRUDRepository buchungsPersonCRUDRepository;
    @Autowired
    MitArbeiterCRUDRepository mitArbeiterCRUDRepository;
    @Autowired
    BuchungsMitReisendeCRUDRepository buchungsMitReisendeCRUDRepository;
    @Autowired
    ZimmerCRUDRepository zimmerCRUDRepository;
    @Autowired
    BuchungsUserService buchungsUserService;
    @Autowired
    HotelCRUDRepository hotelCRUDRepository;
    @Autowired
    BuchungCRUDRepository buchungCRUDRepository;

    /**
     * This the Main class with all my dummy-data
     *
     * @param args the dummy-data running programm
     * @author Lytwynenko Kostjatin
     * @version 1.0
     */
    public static void main(String[] args) {
        SpringApplication.run(VerwaltungsBuchungsSystemVersion2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {


        List<BuchungsPerson> personList = new ArrayList<>();
        /**
         * Create a Hotel
         */
        hotelCRUDRepository.save(new Hotel("Mallorca OceanSide", LocalDate.of(LocalDate.now().getYear(), 6, 1), LocalDate.of(LocalDate.now().getYear(), 8, 31)));


/**
 * Here are the Dummy-Data for the persona(Booking person and Workers).
 */
        for (int i = 0; i < 100; i++) {
            String[] vornamen = new String[]{"Alex", "Thomas", "Tom", "Simon", "Konstantin", "Patrik", "Peter"};
            String[] nachnamen = new String[]{"Bauer", "Horst", "Schneider", "Wagner", "Metzker", "Müller", "Huber"};
            //String[] gebTage = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
            //String[] gebMonate = new String[]{". Jän.",". Feb.",". Mär.",". Apr.",". Mai.",". Jun.",". Jul.",". Aug.",". Sep.",". Okt",". Nov.",". Dez."};

            int zufallVorname = (int) (Math.random() * vornamen.length);
            int zufallNachname = (int) (Math.random() * nachnamen.length);
            int zufallGeschlecht = (int) (Math.random() * 3);
            int zufallAbteilung = (int) (Math.random() * 5);
            //int zufallgebTage = (int) (Math.random()*gebTage.length);
            //int zufallgebMonate = (int) (Math.random()*gebMonate.length);
            Geschlecht geschlecht;
            Abteilung abteilung;

            switch (zufallGeschlecht) {
                case 0:
                    geschlecht = Geschlecht.DIVERSE;
                    break;
                case 1:
                    geschlecht = Geschlecht.MÄNNLICH;
                    break;
                default:
                    geschlecht = Geschlecht.WEIBLICH;
                    break;
            }
            switch (zufallAbteilung) {
                case 0:
                    abteilung = Abteilung.FACILITY;
                    break;
                case 1:
                    abteilung = Abteilung.MANAGEMENT;
                    break;
                case 2:
                    abteilung = Abteilung.REZEPTION;
                    break;
                default:
                    abteilung = Abteilung.SERVICE;
                    break;
            }

            String vorname = vornamen[zufallVorname];
            String nachname = nachnamen[zufallNachname];
/**
 * A Set of Co-Travel.
 */
            Set<BuchungsMitReisende> buchungsMitreisendeSet = new HashSet<>();
            for (int j = 0; j < (int) (Math.random() * 6); j++) {
                BuchungsMitReisende buchungsMitReisende = new BuchungsMitReisende(vorname, nachname, LocalDate.of((int) ((Math.random() * 22) + 2000), ((int) (Math.random() * 12) + 1), (int) (Math.random() * 28) + 1));
                buchungsMitReisende = buchungsUserService.addZusatzinformationen(buchungsMitReisende);
                buchungsMitreisendeSet.add(buchungsMitReisende);
            }

            String eMailAdresse = vorname.toLowerCase() + "_" + nachname + (Math.random() * 1000000) + "@gmail.com";
            String wohnAdresse = nachname + " Straße103";
            LocalDate gebDatum = LocalDate.of((int) ((Math.random() * 60) + 1960), ((int) (Math.random() * 12) + 1), (int) (Math.random() * 28) + 1);
            String kreditKarteNummmer = "1000905076";
            String bankVerbindung = nachname + "_BANK.at";
            String sozialVersicherung = String.valueOf((int) (Math.random() * 9999));
            int gehalt = (int) ((Math.random() * 2500) + 1200);
            LocalDate einstellDatum = LocalDate.of((int) ((Math.random() * 22) + 2000), ((int) (Math.random() * 12) + 1), (int) (Math.random() * 28) + 1);
            int telNummer = (int) (Math.random() * 99999999);

/**
 * Creating BookingsPersonas & Employees.
 */
            try {
                BuchungsPerson buchungsPerson = new BuchungsPerson(vorname, nachname, eMailAdresse, gebDatum, wohnAdresse, kreditKarteNummmer, buchungsMitreisendeSet);
                personList.add(buchungsPerson);
                buchungsPersonCRUDRepository.save(buchungsPerson);
                mitArbeiterCRUDRepository.save(new MitArbeiter(vorname, nachname, eMailAdresse, gebDatum, wohnAdresse, bankVerbindung,
                        sozialVersicherung, gehalt, geschlecht, abteilung, einstellDatum, telNummer));
                for (BuchungsMitReisende buchungsMitReisende : buchungsMitreisendeSet) {
                    buchungsMitReisende.setBuchungsPerson(buchungsPerson);
                    buchungsMitReisendeCRUDRepository.save(buchungsMitReisende);
                }
            } catch (Exception e) {
                System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
            }
        }
        Set<MitArbeiter> alleMitarbeiterSet = new HashSet<>();
        mitArbeiterCRUDRepository.findAll().forEach(alleMitarbeiterSet::add);
        Hotel hotel = hotelCRUDRepository.findById(1).get();
        hotel.setMitArbeiterSet(alleMitarbeiterSet);
        hotelCRUDRepository.save(hotel);

        for (MitArbeiter m : mitArbeiterCRUDRepository.findAll()) {
            m.setHotel(hotel);
            mitArbeiterCRUDRepository.save(m);
            hotel.addMitarbeiterSet(m);
            hotelCRUDRepository.save(hotel);
        }


        /**
         * The Set of Extras what the Rooms got.
         */
        Set<ZimmerZusatz> zusatzSetZimmer1 = new HashSet<>();
        zusatzSetZimmer1 = Stream.of(ZimmerZusatz.BALKON, ZimmerZusatz.MEERBLICK).collect(Collectors.toSet());
        Set<ZimmerZusatz> zusatzSetZimmer2;
        zusatzSetZimmer2 = Stream.of(ZimmerZusatz.BALKON, ZimmerZusatz.MEERBLICK).collect(Collectors.toSet());
        Set<ZimmerZusatz> zusatzSetZimmer3;
        zusatzSetZimmer3 = Stream.of(ZimmerZusatz.BALKON, ZimmerZusatz.MEERBLICK, ZimmerZusatz.GARTEN).collect(Collectors.toSet());
        Set<ZimmerZusatz> zusatzSetZimmer4;
        zusatzSetZimmer4 = Stream.of(ZimmerZusatz.BALKON, ZimmerZusatz.MEERBLICK, ZimmerZusatz.BALKON).collect(Collectors.toSet());
/**
 * I create 24 Rooms for the Hotel
 */
        Zimmer zimmer1 = new Zimmer("RosenZimmer", ZimmerArten.EINZELZIMMER, zusatzSetZimmer1, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer2 = new Zimmer("TulpenZimmer", ZimmerArten.EINZELZIMMER, zusatzSetZimmer1, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer3 = new Zimmer("DoppelRosenZimmer", ZimmerArten.DOPPELZIMMER, zusatzSetZimmer2, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer4 = new Zimmer("DoppelTulpenZimmer", ZimmerArten.DOPPELZIMMER, zusatzSetZimmer2, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer5 = new Zimmer("JuniorRosenZimmer", ZimmerArten.JUNIORSUITE, zusatzSetZimmer3, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer6 = new Zimmer("JuniorTulpenZimmer", ZimmerArten.JUNIORSUITE, zusatzSetZimmer3, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer7 = new Zimmer("SeniorRosenZimmer", ZimmerArten.SENIORSUITE, zusatzSetZimmer4, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer8 = new Zimmer("SeniorTulpenZimmer", ZimmerArten.SENIORSUITE, zusatzSetZimmer4, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer9 = new Zimmer("BärlauchZimmer", ZimmerArten.EINZELZIMMER, zusatzSetZimmer1, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer10 = new Zimmer("MaiglöckchenZimmer", ZimmerArten.EINZELZIMMER, zusatzSetZimmer1, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer11 = new Zimmer("DoppelBärlauchZimmer", ZimmerArten.DOPPELZIMMER, zusatzSetZimmer2, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer12 = new Zimmer("DoppelMaiglöckchenZimmer", ZimmerArten.DOPPELZIMMER, zusatzSetZimmer2, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer13 = new Zimmer("JuniorBärlauchZimmer", ZimmerArten.JUNIORSUITE, zusatzSetZimmer3, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer14 = new Zimmer("JuniorMaiglöckchenZimmer", ZimmerArten.JUNIORSUITE, zusatzSetZimmer3, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer15 = new Zimmer("SeniorBärlauchZimmer", ZimmerArten.SENIORSUITE, zusatzSetZimmer4, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer16 = new Zimmer("SeniorMaiglöckchenZimmer", ZimmerArten.SENIORSUITE, zusatzSetZimmer4, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer17 = new Zimmer("LilienZimmer", ZimmerArten.EINZELZIMMER, zusatzSetZimmer1, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer18 = new Zimmer("OrichideeZimmer", ZimmerArten.EINZELZIMMER, zusatzSetZimmer1, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer19 = new Zimmer("DoppelLilienZimmer", ZimmerArten.DOPPELZIMMER, zusatzSetZimmer2, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer20 = new Zimmer("DoppelOrichideeZimmer", ZimmerArten.DOPPELZIMMER, zusatzSetZimmer2, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer21 = new Zimmer("JuniorLilienZimmer", ZimmerArten.JUNIORSUITE, zusatzSetZimmer3, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer22 = new Zimmer("JuniorOrichideeZimmer", ZimmerArten.JUNIORSUITE, zusatzSetZimmer3, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer23 = new Zimmer("SeniorLilienZimmer", ZimmerArten.SENIORSUITE, zusatzSetZimmer4, hotelCRUDRepository.findById(1).get());
        Zimmer zimmer24 = new Zimmer("SeniorOrichideeZimmer", ZimmerArten.SENIORSUITE, zusatzSetZimmer4, hotelCRUDRepository.findById(1).get());

        Zimmer[] zimmers = new Zimmer[]{zimmer1, zimmer2, zimmer3, zimmer4, zimmer5, zimmer6, zimmer7, zimmer8, zimmer9, zimmer10, zimmer11, zimmer12, zimmer13, zimmer14, zimmer15, zimmer16, zimmer17, zimmer18, zimmer19, zimmer20, zimmer21, zimmer22, zimmer23, zimmer24};

        for (int z = 0; z < 100; z++) {

            Zimmer zimmer = zimmers[(int) (Math.random() * zimmers.length)];

            try {
                zimmerCRUDRepository.save(zimmer);

            } catch (Exception e) {
                System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
            }
        }


        Set<Extras> extras = new HashSet<>();
        extras.add(Extras.AIRPORTSERVICE);
        extras.add(Extras.HOCHZEITSREISE);
/**
 * Few Bookings
 */
        Buchung buchung = new Buchung(Verpflegung.ALLINCLUSIVE, new GregorianCalendar(2022, Calendar.JUNE, 10).getTime(), new GregorianCalendar(2022, Calendar.JUNE, 18).getTime(), personList.get(0), extras);
        int buchungsId = buchungCRUDRepository.save(buchung).getBuchungsId();
        buchungsUserService.setZimmerBuchung(buchungsId, zimmer1.getId());
        buchungsUserService.setZimmerBuchung(buchungsId, zimmer2.getId());
        buchungsUserService.setZimmerBuchung(buchungsId, zimmer3.getId());
        buchungsUserService.setZimmerBuchung(buchungsId, zimmer4.getId());
        buchungCRUDRepository.save(buchung);

        Buchung buchung1 = new Buchung(Verpflegung.HALBPENSION, new GregorianCalendar(2022, Calendar.JULY, 10).getTime(), new GregorianCalendar(2022, Calendar.JULY, 17).getTime(), personList.get(1), extras);
        int buchungs1Id = buchungCRUDRepository.save(buchung1).getBuchungsId();
        buchungsUserService.setZimmerBuchung(buchungs1Id, zimmer5.getId());
        buchungsUserService.setZimmerBuchung(buchungs1Id, zimmer6.getId());
        buchungsUserService.setZimmerBuchung(buchungs1Id, zimmer7.getId());
        buchungsUserService.setZimmerBuchung(buchungs1Id, zimmer8.getId());
        buchungCRUDRepository.save(buchung1);

        Buchung buchung2 = new Buchung(Verpflegung.FRUEHSTUECK, new GregorianCalendar(2022, Calendar.AUGUST, 11).getTime(), new GregorianCalendar(2022, Calendar.FEBRUARY, 18).getTime(), personList.get(2), extras);
        int buchungs2Id = buchungCRUDRepository.save(buchung2).getBuchungsId();
        buchungsUserService.setZimmerBuchung(buchungs2Id, zimmer9.getId());
        buchungsUserService.setZimmerBuchung(buchungs2Id, zimmer10.getId());
        buchungsUserService.setZimmerBuchung(buchungs2Id, zimmer11.getId());
        buchungsUserService.setZimmerBuchung(buchungs2Id, zimmer12.getId());
        buchungCRUDRepository.save(buchung2);

        Buchung buchung3 = new Buchung(Verpflegung.ALLINCLUSIVE, new GregorianCalendar(2022, Calendar.AUGUST, 11).getTime(), new GregorianCalendar(2022, Calendar.AUGUST, 18).getTime(), personList.get(3), extras);
        int buchungs3Id = buchungCRUDRepository.save(buchung3).getBuchungsId();
        buchungsUserService.setZimmerBuchung(buchungs3Id, zimmer13.getId());
        buchungsUserService.setZimmerBuchung(buchungs3Id, zimmer14.getId());
        buchungsUserService.setZimmerBuchung(buchungs3Id, zimmer15.getId());
        buchungsUserService.setZimmerBuchung(buchungs3Id, zimmer16.getId());
        buchungCRUDRepository.save(buchung3);

    }
}


package com.example.VerwaltungsBuchungsSystemVersion2.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int hotelId;

    @Column(nullable = false)
    private String nameHotel;

    @OneToMany(mappedBy = "hotel")
    @JsonManagedReference(value = "mitArbeiter")
    private Set<MitArbeiter> mitArbeiterSet=new HashSet<>();

    @OneToMany(mappedBy = "hotel")
    @JsonManagedReference(value = "zimmer")
    private Set<Zimmer>zimmerAnzahlSet=new HashSet<>();

    @Column(nullable = true)
    private int ausgabe;

    @Column(nullable = true)
    private int einnahmen;

    @Column(nullable = true)
    private LocalDate saisonStart;

    @Column(nullable = true)
    private LocalDate saisonEnde;


    public Hotel(String nameHotel) {
        this.nameHotel = nameHotel;
    }

    public void addMitarbeiterSet(MitArbeiter mitArbeiter){this.mitArbeiterSet.add(mitArbeiter);}

    public void addZimmerAnzahlSet(Zimmer zimmer){this.zimmerAnzahlSet.add(zimmer);}

    public Hotel(String nameHotel, LocalDate saisonStart, LocalDate saisonEnde) {
        this.nameHotel = nameHotel;
        this.saisonStart = saisonStart;
        this.saisonEnde = saisonEnde;
    }
}

package com.example.VerwaltungsBuchungsSystemVersion2.entities;

import com.example.VerwaltungsBuchungsSystemVersion2.enums.ZimmerArten;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.ZimmerZusatz;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Zimmer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String zimmerName;

    @Enumerated(EnumType.STRING)
    private ZimmerArten zimmerArten;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<ZimmerZusatz> zimmerZusatzSet = new HashSet<>();

    @Column(nullable = false)
    private int zimmerPreis;

    @ManyToOne
    @JsonBackReference(value = "zimmer")
    @JoinColumn(name = "hotelId")
    private Hotel hotel;

    @OneToMany(mappedBy = "zimmer")
    @JsonManagedReference(value = "buchungsZimmer")
    private Set<BuchungZimmer> buchungZimmerSet = new HashSet<>();


    public Zimmer(String zimmerName, ZimmerArten zimmerArten, Set<ZimmerZusatz> zimmerZusatzSet, Hotel hotel) {
        this.zimmerName = zimmerName;
        this.zimmerArten = zimmerArten;
        this.zimmerZusatzSet = zimmerZusatzSet;
        this.zimmerPreis = zimmerArten.getPreisProZimmer();
        this.hotel = hotel;
    }


    public void setZimmerZusatz(ZimmerZusatz zimmerZusatz) {
        this.zimmerZusatzSet.add(zimmerZusatz);
    }

    public void removeZimmerZusatz(ZimmerZusatz zimmerZusatz) {
        if (this.zimmerZusatzSet.contains(zimmerZusatz)) {
            this.zimmerZusatzSet.remove(zimmerZusatz);
        }
    }

    @Override
    public String toString() {
        return "Zimmer{" +
                "id=" + id +
                ", nameZimmer='" + zimmerName + '\'' +
                ", zimmerArten=" + zimmerArten +
                ", zimmerPreis=" + zimmerPreis +
                ", hotel=" + hotel +
                '}';
    }

    public void addBuchungZimmer(BuchungZimmer buchungZimmer) {
        buchungZimmerSet = new HashSet<>();
        buchungZimmerSet.add(buchungZimmer);
    }
}

package com.example.VerwaltungsBuchungsSystemVersion2.entities;


import com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost.BuchungsMitreisendenDTOBuchung;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class BuchungsMitReisende {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String vornameBMR;

    @Column(nullable = false)
    private String nachnameBMR;

    @Column(nullable = false)
    private LocalDate alterBMR;

    @Column(nullable = false)
    private boolean kinderBett;

    @Column(nullable = false)
    private double preisRabatt;

    @ManyToOne
    @JsonBackReference(value = "buchungsMitreisende")
    @JoinColumn(name = "buchungsPerson_id")
    private BuchungsPerson buchungsPerson;

    public BuchungsMitReisende(BuchungsMitreisendenDTOBuchung buchungsMitreisendenDTOBuchung) {
        this.vornameBMR = buchungsMitreisendenDTOBuchung.getVorName();
        this.nachnameBMR = buchungsMitreisendenDTOBuchung.getNachName();
        this.alterBMR = buchungsMitreisendenDTOBuchung.getGeburtsTag();
        this.kinderBett = buchungsMitreisendenDTOBuchung.isKinderBett();
        this.preisRabatt = buchungsMitreisendenDTOBuchung.getPreisRabatt();
    }


    public BuchungsMitReisende(String vorNameBMR, String nachNameBMR, LocalDate alterBMR) {
        this.vornameBMR = vorNameBMR;
        this.nachnameBMR = nachNameBMR;
        this.alterBMR = alterBMR;
    }

}

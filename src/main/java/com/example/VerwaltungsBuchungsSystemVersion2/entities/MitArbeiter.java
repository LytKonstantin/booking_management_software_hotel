package com.example.VerwaltungsBuchungsSystemVersion2.entities;

import com.example.VerwaltungsBuchungsSystemVersion2.enums.Abteilung;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Geschlecht;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "mitArbeiter")
public class MitArbeiter extends Person {

    @ManyToOne
    @JsonBackReference(value = "mitArbeiter")
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    @Column(nullable = false)
    private String bankVerbindung;

    @Column(nullable = false)
    private String sozialVersicherung;

    @Column(nullable = false)
    private int gehalt;

    @Enumerated(EnumType.STRING)
    private Geschlecht geschlecht;

    @Enumerated(EnumType.STRING)
    private Abteilung abteilung;

    @Column(nullable = false)
    private LocalDate einstellDatum;

    @Column(nullable = false)
    private int telNummer;


    public MitArbeiter(String vorname, String nachName, String eMailAdresse,
                       LocalDate gebDatum, String wohnAdresse, String bankVerbindung,
                       String sozialVersicherung, int gehalt, Geschlecht geschlecht,
                       Abteilung abteilung, LocalDate einstellDatum, int telNummer) {
        super(vorname, nachName, eMailAdresse, gebDatum, wohnAdresse);
        this.bankVerbindung = bankVerbindung;
        this.sozialVersicherung = sozialVersicherung;
        this.gehalt = gehalt;
        this.geschlecht = geschlecht;
        this.abteilung = abteilung;
        this.einstellDatum = einstellDatum;
        this.telNummer = telNummer;
    }
}

package com.example.VerwaltungsBuchungsSystemVersion2.entities;


import com.example.VerwaltungsBuchungsSystemVersion2.enums.Extras;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Verpflegung;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Buchung {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int buchungsId;

    @Column
    private int personenAnzahlBuchung;

    @Column
    private double preisBerechungBuchung;

    @Enumerated(EnumType.STRING)
    private Verpflegung verpflegung;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<Extras> extras;

    @Column
    private LocalDate buchungsdatum;


    @Column(nullable = false)
    private Date anreiseDatum;

    @Column(nullable = false)
    private Date abreiseDatum;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "buchung")
    @JsonManagedReference(value = "BuchungsZimmer")
    private Set<BuchungZimmer> buchungZimmerSet = new HashSet<>();

    @ManyToOne
    @JsonBackReference(value = "buchung")
    @JoinColumn(name = "buchungsPerson_id")
    private BuchungsPerson buchungsPerson;

    public Buchung(Date anreiseDatum, Date abreiseDatum) {
        this.anreiseDatum = anreiseDatum;
        this.abreiseDatum = abreiseDatum;
    }

    public Buchung(Verpflegung verpflegung, Date anreiseDatum, Date abreiseDatum, BuchungsPerson buchungsPerson, Set<Extras> extras) {
        this.verpflegung = verpflegung;
        this.anreiseDatum = anreiseDatum;
        this.abreiseDatum = abreiseDatum;
        this.buchungsPerson = buchungsPerson;
        this.extras = extras;
        this.personenAnzahlBuchung = buchungsPerson.getBuchungsMitReisendeSet().size() + 1;
        this.buchungsdatum = LocalDate.now();
        this.preisBerechungBuchung = preisBerechungBuchung;
    }

    public void addBuchungZimmer(BuchungZimmer buchungZimmer) {
        buchungZimmerSet.add(buchungZimmer);
    }
}

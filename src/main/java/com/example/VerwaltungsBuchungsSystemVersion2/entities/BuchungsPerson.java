package com.example.VerwaltungsBuchungsSystemVersion2.entities;


import com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost.BuchungsMitreisendenDTOBuchung;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost.KompletteBuchungDTOPost;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "buchungsPerson")
public class BuchungsPerson extends Person {

    @Column(nullable = false)
    private String kreditKarteNummer;

    @OneToMany(mappedBy = "buchungsPerson")
    @JsonManagedReference(value = "buchungsMitreisende")
    private Set<BuchungsMitReisende> buchungsMitReisendeSet = new HashSet<>();

    @Column(nullable = false)
    private String buchungsNummer = getNachname() + "_Buchnugsnummer = 0" + getGebDatum();


    @OneToMany(mappedBy = "buchungsPerson")
    @JsonManagedReference(value = "buchung")
    private Set<Buchung> buchungSet = new LinkedHashSet<>();



    public BuchungsPerson(KompletteBuchungDTOPost buchungsPersonDTOBuchung){
        super(buchungsPersonDTOBuchung.getVorName(), buchungsPersonDTOBuchung.getNachName(),
                buchungsPersonDTOBuchung.getEMailAdresse(),buchungsPersonDTOBuchung.getGeburtsDatum(),
                buchungsPersonDTOBuchung.getWohnAdresse());
        this.kreditKarteNummer = buchungsPersonDTOBuchung.getKredtKartenNummer();
    }

    public BuchungsPerson(String vorname, String nachname, String eMailAdresse, LocalDate gebDatum, String wohnAdresse, String kreditKarteNummer, Set<BuchungsMitReisende> buchungsMitReisendeSet) {
        super(vorname, nachname, eMailAdresse, gebDatum, wohnAdresse);
        this.kreditKarteNummer = kreditKarteNummer;
        this.buchungsMitReisendeSet = buchungsMitReisendeSet;
    }

    public void addBuchungsMitReisendeSet(BuchungsMitReisende buchungsMitReisende) {
        this.buchungsMitReisendeSet.add(buchungsMitReisende);
    }

    public void addBuchungSet(Buchung buchung) {
        this.buchungSet.add(buchung);
    }

}

package com.example.VerwaltungsBuchungsSystemVersion2.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class BuchungZimmer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int buchungsZimmer_id;

    @ManyToOne
    @JsonBackReference(value = "buchungsZimmer")
    @JoinColumn(name = "zimmer_id")
    private Zimmer zimmer;

    @ManyToOne
    @JsonBackReference(value = "BuchungsZimmer")
    @JoinColumn(name = "buchung_id")
    private Buchung buchung;


    private final GregorianCalendar PREIS_ERHOEHUNG_AB = new GregorianCalendar(LocalDate.now().getYear(), Calendar.JUNE, 1);
    private final GregorianCalendar PREIS_ERHOEHUNG_BIS = new GregorianCalendar(LocalDate.now().getYear(), Calendar.AUGUST, 31);

    public BuchungZimmer(Buchung buchung, Zimmer zimmer) {
        this.buchung = buchung;
        this.zimmer = zimmer;
    }
}

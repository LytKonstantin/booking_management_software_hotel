package com.example.VerwaltungsBuchungsSystemVersion2.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;

    @Column(nullable = false)
    private String vorname;

    @Column(nullable = false)
    private String nachname;

    @Column(nullable = false)
    private String eMailAdresse;

    @Column(nullable = false)
    private LocalDate gebDatum;

    @Column(nullable = false)
    private String wohnAdresse;

    public Person(String vorname, String nachname, String eMailAdresse, LocalDate gebDatum, String wohnAdresse) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.eMailAdresse = eMailAdresse;
        this.gebDatum = gebDatum;
        this.wohnAdresse = wohnAdresse;
    }
}

package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
public class ZeitraumDTO {
    private Date von;
    private Date bis;

    public static LocalDate convertToLocalDateViaInstant(@NotNull Date dateToConvert) {
        return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Instant convertToDateViaInstant(@NotNull LocalDate localDateToConvert) {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        return localDateToConvert.atStartOfDay(defaultZoneId).toInstant();
    }


    public static int zeitSpanneInTagen(Date von, Date bis) {
        LocalDate buchungsStart = ZeitraumDTO.convertToLocalDateViaInstant(von);
        LocalDate buchungsEnde = ZeitraumDTO.convertToLocalDateViaInstant(bis);
        return (int) buchungsStart.until(buchungsEnde, ChronoUnit.DAYS);
    }
}

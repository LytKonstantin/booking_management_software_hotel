package com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
/**
 * This Co-TravelDTO is making a Co-Travel and added Extras to them.
 */
public class BuchungsMitreisendenDTOBuchung {

    /**
     * is the firstname of the Co-traveler.
     */
    private String vorName;
    /**
     * is the Lastname of the Co-traveler.
     */
    private String nachName;
    /**
     * is the Birthday of the Co-traveler.
     */
    private LocalDate geburtsTag;
    /**
     * If Co-traveler get an extra Bed for Kids but this will be calculated with the Age.
     */
    private boolean kinderBett;
    /**
     * If Co-traveler get a price-discount will be calculated with the Age.
     */
    private double preisRabatt;

    /**
     * Class constructor
     *
     * @param vorName    is the firstname of the Co-traveler.
     * @param nachName   is the Lastname of the Co-traveler.
     * @param geburtsTag is the Birthday of the Co-traveler.
     */
    public BuchungsMitreisendenDTOBuchung(String vorName, String nachName, LocalDate geburtsTag) {
        this.vorName = vorName;
        this.nachName = nachName;
        this.geburtsTag = geburtsTag;
        this.kinderBett = addKinderbett(geburtsTag);
        this.preisRabatt = addPreisRabatt(geburtsTag);
    }

    /**
     * @param geburtsTag Birthday will calculate if the Co-travel got a Children Bed.
     * @return boolean Children Bed
     */
    private boolean addKinderbett(LocalDate geburtsTag) {
        int alter = LocalDate.now().compareTo(geburtsTag);
        if (alter < 14) {
            setKinderBett(true);
        } else {
            setKinderBett(false);
        }
        return kinderBett;
    }

    /**
     * @param geburtsTag Birthday will calculate if the Co-travel got Price-Discount.
     * @return double price-discount.
     */
    private double addPreisRabatt(LocalDate geburtsTag) {
        int alter = LocalDate.now().compareTo(geburtsTag);
        if (alter < 7) {
            setPreisRabatt(0.0);
        } else if (alter < 14) {
            setPreisRabatt(0.5);
        } else {
            setPreisRabatt(1.0);
        }
        return preisRabatt;
    }

}


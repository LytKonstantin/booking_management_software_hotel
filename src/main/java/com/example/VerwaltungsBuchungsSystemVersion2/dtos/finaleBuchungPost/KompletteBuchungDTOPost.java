package com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class KompletteBuchungDTOPost {

    private String vorName;
    private String nachName;
    private LocalDate geburtsDatum;
    private String eMailAdresse;
    private String wohnAdresse;
    private String kredtKartenNummer;

    private Set<BuchungsMitreisendenDTOBuchung> buchungsMitreisendenDTOBuchungSet;

    private Set<Integer> zimmerIds;

    private LocalDate anreiseDatum;
    private LocalDate abreiseDatum;


    private String verpflegung;
    private Set<String> extras;

}

package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
@AllArgsConstructor
public class StornoDTO {

    private int buchungsNummer;
    private double gesamtPreis;
    private double preisDerErstattung;
    private LocalDate buchungsdatum;
    private LocalDate stornodatum;
    private LocalDate anreiseDatum;

}

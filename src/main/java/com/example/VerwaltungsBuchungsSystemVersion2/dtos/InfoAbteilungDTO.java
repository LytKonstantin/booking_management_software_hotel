package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InfoAbteilungDTO {

    private String abteilung;
    private int anzahl;
    private int gehalt;

}

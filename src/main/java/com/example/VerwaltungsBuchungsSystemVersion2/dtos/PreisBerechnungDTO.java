package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PreisBerechnungDTO {

    private int preisDerZimmernProTag;
    private double preisAllerZimmern;
    private double preisVonVerpfegung;
    private int personAnzahlGesamt;
    private int anzahlDerGebuchtenTage;
    private double gesamtPreis;


}

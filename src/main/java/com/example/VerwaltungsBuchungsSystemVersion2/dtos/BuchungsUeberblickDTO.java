package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class BuchungsUeberblickDTO {

    private Date anreiseDatum;
    private Date abreiseDatum;
    private int personenAnzahl;
    private int kinderbetter;
    private List gebuchterZimmer;

}

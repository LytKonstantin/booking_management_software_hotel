package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BilanzDTO {

    private int anzahlDerAngestellten;
    private int gehaltDerAngestellten;
    private int anzahlDerBuchungen;
    private int einnahmenDerBuchungen;
    private int Bilanz;
}

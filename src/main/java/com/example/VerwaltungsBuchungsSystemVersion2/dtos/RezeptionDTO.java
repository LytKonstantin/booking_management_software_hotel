package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.Buchung;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.Zimmer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class RezeptionDTO {

    private List<Buchung> alleBuchungen;
    private List<Zimmer> alleZimmer;

}

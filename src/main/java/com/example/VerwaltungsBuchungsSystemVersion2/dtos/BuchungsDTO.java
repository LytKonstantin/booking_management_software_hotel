package com.example.VerwaltungsBuchungsSystemVersion2.dtos;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.BuchungsMitReisende;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.BuchungsPerson;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.Zimmer;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Extras;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class BuchungsDTO {

    private ZeitraumDTO zeitraumDTO;

    private BuchungsPerson buchungsPerson;
    private Set<BuchungsMitReisende> buchungsMitReisendeSet;

    private List<Zimmer> gebuchteZimmer;

    private String verpflegung;
    private List<Set<Extras>> extras;

    private int gebuchteTage;

    private double preis;


}

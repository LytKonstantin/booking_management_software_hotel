package com.example.VerwaltungsBuchungsSystemVersion2.services;


import com.example.VerwaltungsBuchungsSystemVersion2.dtos.BilanzDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.InfoAbteilungDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.*;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Abteilung;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Geschlecht;
import com.example.VerwaltungsBuchungsSystemVersion2.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;



/**
 * The Service-Class of The Hotel for Employees
 */
@Service
public class AdminHotelService {

    @Autowired
    HotelCRUDRepository hotelCRUDRepository;
    @Autowired
    MitArbeiterCRUDRepository mitArbeiterCRUDRepository;
    @Autowired
    ZimmerCRUDRepository zimmerCRUDRepository;
    @Autowired
    BuchungCRUDRepository buchungCRUDRepository;
    @Autowired
    BuchungsUserService buchungsUserService;
    @Autowired
    BuchungsPersonCRUDRepository buchungsPersonCRUDRepository;

    /**
     * Deleting process of an Employee.
     *
     * @param mitArbeiterId Need the EmployeeId to delete the Employee.
     */
    public void deleteMitArbeiter(int mitArbeiterId) {
        mitArbeiterCRUDRepository.deleteById(mitArbeiterId);
    }

    /**
     * Deleting process of a Room.
     *
     * @param zimmerId Need the RoomId to delete the Room.
     */
    public void deleteZimmer(int zimmerId) {
        zimmerCRUDRepository.deleteById(zimmerId);
    }

    public void deleteBuchung(int buchungId) {
        buchungCRUDRepository.deleteById(buchungId);
    }

    /**
     * The Method get a List of Employees that work in the Hotel.
     *
     * @return List of all Employees.
     */
    public List<MitArbeiter> getALLMitarbeiter() {
        Iterable<MitArbeiter> aLLMitarbeter = mitArbeiterCRUDRepository.findAll();
        List<MitArbeiter> mitArbeiterList = new ArrayList<>();
        aLLMitarbeter.forEach(mitArbeiterList::add);

        return mitArbeiterList;
    }

    public List<MitArbeiter> getALLMitarbeiterPerAbteilung(String abteilung) {
        Iterable<MitArbeiter> aLLMitarbeiterPerAbteilung = mitArbeiterCRUDRepository.findByAbteilungIs(Abteilung.valueOf(abteilung.toUpperCase()));
        List<MitArbeiter> mitArbeiterList = new ArrayList<>();
        aLLMitarbeiterPerAbteilung.forEach(mitArbeiterList::add);

        return mitArbeiterList;
    }

    public List<MitArbeiter> getALLMitarbeiterPerGeschlecht(String geschlecht) {
        Iterable<MitArbeiter> aLLMitarbeiterPerGeschlecht = mitArbeiterCRUDRepository.findByGeschlechtIs(Geschlecht.valueOf(geschlecht.toUpperCase()));
        List<MitArbeiter> mitArbeiterList = new ArrayList<>();
        aLLMitarbeiterPerGeschlecht.forEach(mitArbeiterList::add);

        return mitArbeiterList;
    }

    public int getMitarbeiterAnzahl() {
        return getALLMitarbeiter().size();
    }

    public int getGehalt(List<MitArbeiter> mitArbeiterList) {
        return mitArbeiterList.stream().mapToInt(MitArbeiter::getGehalt).sum();
    }

    public Set<InfoAbteilungDTO> getInfoAbteilung() {
        Set<InfoAbteilungDTO> infoAbteilungDTOS = new HashSet<>();
        Arrays.stream(Abteilung.values()).map(e -> infoAbteilungDTOS.add(new InfoAbteilungDTO(e.toString(), mitArbeiterCRUDRepository.
                findByAbteilungIs(e).size(), getGehalt(mitArbeiterCRUDRepository.findByAbteilungIs(e))))).collect(Collectors.toSet());

        return infoAbteilungDTOS;
    }


    public MitArbeiter getMitArbeiter(int mitArbeiterId) {
        return mitArbeiterCRUDRepository.findById(mitArbeiterId).get();
    }

    public Zimmer getZimmer(int zimmerId) {
        return zimmerCRUDRepository.findById(zimmerId).get();
    }

    public Buchung getBuchung(int buchungId) {
        return buchungCRUDRepository.findById(buchungId).get();
    }

    public void post(MitArbeiter mitArbeiter) {
        mitArbeiterCRUDRepository.save(mitArbeiter);
    }

    public void post(Zimmer zimmer) {
        zimmerCRUDRepository.save(zimmer);
    }

    public void post(Hotel hotel) {
        hotelCRUDRepository.save(hotel);
    }

    public void put(MitArbeiter mitArbeiter, int mitArbeiterId) {
        mitArbeiterCRUDRepository.findById(mitArbeiterId);
    }

    public void put(Zimmer zimmer, int zimmerId) {
        zimmerCRUDRepository.findById(zimmerId);
    }

    public void put(Buchung buchung, int buchungId) {
        buchungCRUDRepository.save(buchung);
    }

    public BuchungsPerson getBuchungsPerson(int buchungsPersonId) {
        return buchungsPersonCRUDRepository.findById(buchungsPersonId).get();
    }

    public void post(BuchungsPerson buchungsPerson) {
        buchungsPersonCRUDRepository.save(buchungsPerson);
    }

    public void put(BuchungsPerson buchungsPerson, int buchungsPersonId) {
        buchungsPersonCRUDRepository.save(buchungsPerson);
    }


    public List<Buchung> buchungsAnzahl() {
        Iterable<Buchung> alleBuchungen = buchungCRUDRepository.findAll();
        List<Buchung> buchungList = new ArrayList<>();
        alleBuchungen.forEach(buchungList::add);

        return buchungList;
    }

    public BilanzDTO bilanzRechnung(int hotelId) {
        List<MitArbeiter> alleMitarbeiter = getALLMitarbeiter();
        List<Buchung> alleBuchungen = buchungsAnzahl();
        int anzahlDerAngestellten = getMitarbeiterAnzahl();
        int gehaltDerAngestellten = alleMitarbeiter.stream().mapToInt(MitArbeiter::getGehalt).sum();
        hotelCRUDRepository.findById(hotelId).get().setAusgabe(gehaltDerAngestellten);
        int anzahlDerBuchungen = (int) buchungCRUDRepository.count();
        int einnahmenDerBuchungen = (int) alleBuchungen.stream().mapToDouble(b -> buchungsUserService.preisPerBuchung(b.getBuchungsId())).sum();
        hotelCRUDRepository.findById(hotelId).get().setEinnahmen(einnahmenDerBuchungen);
        int bilanz = einnahmenDerBuchungen - gehaltDerAngestellten;

        return new BilanzDTO(anzahlDerAngestellten, gehaltDerAngestellten, anzahlDerBuchungen, einnahmenDerBuchungen, bilanz);
    }

}

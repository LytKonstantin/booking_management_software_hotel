package com.example.VerwaltungsBuchungsSystemVersion2.services;

import com.example.VerwaltungsBuchungsSystemVersion2.dtos.RezeptionDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.ZeitraumDTO;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.Buchung;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.BuchungZimmer;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.Zimmer;
import com.example.VerwaltungsBuchungsSystemVersion2.repositories.BuchungCRUDRepository;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class MitArbeiterService {


    @Autowired
    BuchungCRUDRepository buchungCRUDRepository;


    public void post(Buchung buchung) {
        buchungCRUDRepository.save(buchung);
    }

    public void deleteBuchung(int buchung_id) {
        buchungCRUDRepository.deleteById(buchung_id);
    }

    public RezeptionDTO alleBuchungen(@NotNull ZeitraumDTO zeitraum) {
        Date von = zeitraum.getVon();
        Date bis = zeitraum.getBis();
        List<Buchung> buchungList = (List<Buchung>) buchungCRUDRepository.findAll();
        List<Buchung> b2 = buchungList.stream().filter(b -> (von.before(b.getAbreiseDatum()) && bis.after(b.getAbreiseDatum()))
                        || (von.before(b.getAnreiseDatum()) && bis.after(b.getAnreiseDatum()))
                        || (von.after(b.getAnreiseDatum()) && bis.before(b.getAbreiseDatum()))
                        || (von.before(b.getAnreiseDatum())) && bis.after(b.getAbreiseDatum()))
                .collect(Collectors.toList());
        List<Zimmer> zimmerList = b2.stream()
                .flatMap(b -> b.getBuchungZimmerSet().stream().map(BuchungZimmer::getZimmer)
                        .collect(Collectors.toSet()).stream()).distinct().collect(Collectors.toList());


        return new RezeptionDTO(b2, zimmerList);
    }

}

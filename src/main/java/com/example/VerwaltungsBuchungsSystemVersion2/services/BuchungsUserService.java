package com.example.VerwaltungsBuchungsSystemVersion2.services;

import com.example.VerwaltungsBuchungsSystemVersion2.dtos.*;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost.BuchungsMitreisendenDTOBuchung;
import com.example.VerwaltungsBuchungsSystemVersion2.dtos.finaleBuchungPost.KompletteBuchungDTOPost;
import com.example.VerwaltungsBuchungsSystemVersion2.entities.*;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Extras;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Verpflegung;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.ZimmerArten;
import com.example.VerwaltungsBuchungsSystemVersion2.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class BuchungsUserService {

    @Autowired
    BuchungCRUDRepository buchungCRUDRepository;
    @Autowired
    BuchungsMitReisendeCRUDRepository buchungsMitReisendeCRUDRepository;
    @Autowired
    BuchungZimmerCRUDRepository buchungZimmerCRUDRepository;
    @Autowired
    ZimmerCRUDRepository zimmerCRUDRepository;
    @Autowired
    HotelCRUDRepository hotelCRUDRepository;
    @Autowired
    BuchungsPersonCRUDRepository buchungsPersonCRUDRepository;

    public void delete(int buchung_id) {
        buchungCRUDRepository.deleteById(buchung_id);
    }

    public void setZimmerBuchung(Integer buchungId, Integer zimmerId) {
        Buchung buchung = buchungCRUDRepository.findById(buchungId).get();
        Zimmer zimmer = zimmerCRUDRepository.findById(zimmerId).get();

        BuchungZimmer buchungZimmer = new BuchungZimmer(buchung, zimmer);
        buchungZimmer = buchungZimmerCRUDRepository.save(buchungZimmer);

        buchung.addBuchungZimmer(buchungZimmer);
        zimmer.addBuchungZimmer(buchungZimmer);

        zimmerCRUDRepository.save(zimmer);
        buchungCRUDRepository.save(buchung);

    }

    public Buchung getBuchung(int buchungId) {
        return buchungCRUDRepository.findById(buchungId).get();
    }

    public BuchungsPerson getBuchungsPerson(int buchungspersonId) {
        return buchungsPersonCRUDRepository.findById(buchungspersonId).get();
    }


    public BuchungsMitReisende addZusatzinformationen(BuchungsMitReisende buchungsMitReisende) {
        int alter = LocalDate.now().compareTo(buchungsMitReisende.getAlterBMR());
        if (alter < 7) {
            buchungsMitReisende.setKinderBett(true);
            buchungsMitReisende.setPreisRabatt(0.0);
        } else if (alter < 14) {
            buchungsMitReisende.setKinderBett(true);
            buchungsMitReisende.setPreisRabatt(0.5);
        } else {
            buchungsMitReisende.setKinderBett(false);
            buchungsMitReisende.setPreisRabatt(1);
        }

        return buchungsMitReisende;
    }

    public Map<String, Integer> getAufgelisteteFreieZimmer(ZeitraumDTO zeitraum) {
        List<Zimmer> verfügbareZimmer = getZimmerVerfuegbarkeit(zeitraum);
        HashMap<String, Integer> verfuegbareZimmerHashMap = new HashMap<>();
        verfuegbareZimmerHashMap.put(String.valueOf(ZimmerArten.EINZELZIMMER), Math.toIntExact(verfügbareZimmer.stream().filter(i -> i.getZimmerArten() == ZimmerArten.EINZELZIMMER).count()));
        verfuegbareZimmerHashMap.put(String.valueOf(ZimmerArten.DOPPELZIMMER), Math.toIntExact(verfügbareZimmer.stream().filter(i -> i.getZimmerArten() == ZimmerArten.DOPPELZIMMER).count()));
        verfuegbareZimmerHashMap.put(String.valueOf(ZimmerArten.JUNIORSUITE), Math.toIntExact(verfügbareZimmer.stream().filter(i -> i.getZimmerArten() == ZimmerArten.JUNIORSUITE).count()));
        verfuegbareZimmerHashMap.put(String.valueOf(ZimmerArten.SENIORSUITE), Math.toIntExact(verfügbareZimmer.stream().filter(i -> i.getZimmerArten() == ZimmerArten.SENIORSUITE).count()));

        return verfuegbareZimmerHashMap;
    }

    public List<Zimmer> getZimmerVerfuegbarkeit(ZeitraumDTO zeitraum) {
        List<Buchung> buchungen = (List<Buchung>) buchungCRUDRepository.findAll();
        List<Zimmer> freieZimmer = (List<Zimmer>) zimmerCRUDRepository.findAll();
        Set<Zimmer> belegteZimmer = new HashSet<>();

        for (Buchung buchung : buchungen) {
            if (zeitraum.getVon().before(buchung.getAnreiseDatum()) && zeitraum.getBis().after(buchung.getAnreiseDatum()) ||
                    zeitraum.getVon().after(buchung.getAnreiseDatum()) && zeitraum.getVon().before(buchung.getAbreiseDatum())) {
                for (BuchungZimmer buchungZimmer : buchung.getBuchungZimmerSet()) {
                    belegteZimmer.add(buchungZimmer.getZimmer());
                }
            }
        }

        belegteZimmer.forEach(freieZimmer::remove);
        return freieZimmer;
    }

    private int anzahlKinderBetten(int buchungId) {
        int anzahlKinderBetten = (int) buchungCRUDRepository.findById(buchungId).get().
                getBuchungsPerson().getBuchungsMitReisendeSet().stream().filter(BuchungsMitReisende::isKinderBett).count();
        return anzahlKinderBetten;
    }

    private int allePersonen(int buchungId) {
        int allePersonen = 1 + buchungCRUDRepository.findById(buchungId).get().getBuchungsPerson().getBuchungsMitReisendeSet().size();
        return allePersonen;
    }


    public BuchungsUeberblickDTO getbuchungsUeberblickDTO(int buchungId) {
        List<Zimmer> gebuchteZimmer = new ArrayList<>();
        buchungCRUDRepository.findById(buchungId).get().getBuchungZimmerSet().forEach(z -> gebuchteZimmer.add(z.getZimmer()));
        return new BuchungsUeberblickDTO(buchungCRUDRepository.findById(buchungId).get().
                getAnreiseDatum(), buchungCRUDRepository.findById(buchungId).get().getAbreiseDatum(),
                allePersonen(buchungId), anzahlKinderBetten(buchungId), gebuchteZimmer);
    }


    public PreisBerechnungDTO preisBerechnung(int buchungId, int hotelId) {
        Set<BuchungsMitReisende> allePersonen;
        allePersonen = buchungCRUDRepository.findById(buchungId).get().getBuchungsPerson().getBuchungsMitReisendeSet();
        allePersonen.forEach(this::addZusatzinformationen);

        buchungsMitReisendeCRUDRepository.saveAll(allePersonen);

        int preisDerVerplegung = buchungCRUDRepository.findById(buchungId).get().getVerpflegung().getPreisDerVerplegung();
        double gesamtVerplegung = allePersonen.stream().mapToDouble(e -> e.getPreisRabatt() * preisDerVerplegung).sum() + preisDerVerplegung;
        int preisDerZimmern = buchungCRUDRepository.findById(buchungId).get().getBuchungZimmerSet().stream().mapToInt(e -> e.getZimmer().getZimmerPreis()).sum();
        int zeitSpanne = ZeitraumDTO.zeitSpanneInTagen(buchungCRUDRepository.findById(buchungId).get().getAnreiseDatum(), buchungCRUDRepository.findById(buchungId).get().getAbreiseDatum());
        double preisSaisonDerZimmer = preisErhoehung(buchungId, hotelId);

        double gesamtPreis = preisSaisonDerZimmer + zeitSpanne * gesamtVerplegung;

        return new PreisBerechnungDTO(preisDerZimmern, preisSaisonDerZimmer, gesamtVerplegung, allePersonen.size() + 1, zeitSpanne, gesamtPreis);
    }


    public double preisErhoehung(int buchungId, int hotelId) {
        LocalDate buchungsStart = ZeitraumDTO.convertToLocalDateViaInstant(buchungCRUDRepository.findById(buchungId).get().getAnreiseDatum());
        LocalDate buchungsEnde = ZeitraumDTO.convertToLocalDateViaInstant(buchungCRUDRepository.findById(buchungId).get().getAbreiseDatum());
        LocalDate saisonStart = hotelCRUDRepository.findById(hotelId).get().getSaisonStart();
        LocalDate saisonEnde = hotelCRUDRepository.findById(hotelId).get().getSaisonEnde();
        int zeitSpanne = (int) buchungsStart.until(buchungsEnde, ChronoUnit.DAYS);
        int preisDerZimmern = buchungCRUDRepository.findById(buchungId).get().getBuchungZimmerSet().stream().mapToInt(e -> e.getZimmer().getZimmerPreis()).sum();
        double preis;


        if (buchungsStart.isAfter(saisonStart) && buchungsEnde.isBefore(saisonEnde)) {
            preis = preisDerZimmern * zeitSpanne * 1.1;
        } else if (buchungsStart.isBefore(saisonStart) && buchungsEnde.isAfter(saisonEnde)) {
            int tageAusserhalbDerSaison = (int) buchungsStart.until(saisonStart, ChronoUnit.DAYS) + (int) saisonEnde.until(buchungsEnde, ChronoUnit.DAYS);
            int tageInnerhalbDerSaison = (int) saisonStart.until(saisonEnde, ChronoUnit.DAYS);
            preis = tageAusserhalbDerSaison * preisDerZimmern + tageInnerhalbDerSaison * preisDerZimmern * 1.1;
        } else if (buchungsStart.isBefore(saisonStart) && buchungsEnde.isBefore(saisonEnde)) {
            int tageAusserhalbDerSaison = (int) buchungsStart.until(saisonStart, ChronoUnit.DAYS);
            int tageInnerhalbDerSaison = (int) saisonStart.until(buchungsEnde, ChronoUnit.DAYS);
            preis = tageAusserhalbDerSaison * preisDerZimmern + tageInnerhalbDerSaison * preisDerZimmern * 1.1;
        } else if (saisonStart.isBefore(buchungsStart) && buchungsEnde.isAfter(saisonEnde)) {
            int tageAusserhalbDerSaison = (int) saisonEnde.until(buchungsEnde, ChronoUnit.DAYS);
            int tageInnerhalbDerSaison = (int) buchungsStart.until(saisonEnde, ChronoUnit.DAYS);
            preis = tageAusserhalbDerSaison * preisDerZimmern + tageInnerhalbDerSaison * preisDerZimmern * 1.1;
        } else {
            preis = (int) buchungsStart.until(buchungsEnde, ChronoUnit.DAYS) * preisDerZimmern;
        }
        return preis;
    }

    public double preisPerBuchung(int buchungId) {
        Set<BuchungsMitReisende> allePersonen;
        allePersonen = buchungCRUDRepository.findById(buchungId).get().getBuchungsPerson().getBuchungsMitReisendeSet();
        allePersonen.forEach(this::addZusatzinformationen);
        buchungsMitReisendeCRUDRepository.saveAll(allePersonen);
        int preisDerVerplegung = buchungCRUDRepository.findById(buchungId).get().getVerpflegung().getPreisDerVerplegung();
        double gesamtVerplegung = allePersonen.stream().mapToDouble(e -> e.getPreisRabatt() * preisDerVerplegung).sum() + preisDerVerplegung;
        int preisDerZimmern = buchungCRUDRepository.findById(buchungId).get().getBuchungZimmerSet().stream().mapToInt(e -> e.getZimmer().getZimmerPreis()).sum();
        int zeitSpanne = ZeitraumDTO.zeitSpanneInTagen(buchungCRUDRepository.findById(buchungId).get().getAnreiseDatum(), buchungCRUDRepository.findById(buchungId).get().getAbreiseDatum());

        double preisPerBuchung = zeitSpanne * preisDerZimmern + zeitSpanne * gesamtVerplegung;

        return preisPerBuchung;
    }

    public StornoDTO stornoBerechnung(int buchungId, int hotelId) {
        double gesamtPreis = preisBerechnung(buchungId, hotelId).getGesamtPreis();
        LocalDate stornoDatum = LocalDate.now();
        LocalDate buchungsDatum = buchungCRUDRepository.findById(buchungId).get().getBuchungsdatum();
        LocalDate anreiseDatum = ZeitraumDTO.convertToLocalDateViaInstant(buchungCRUDRepository.findById(buchungId).get().getAnreiseDatum());
        int zeitSpanne1 = (int) stornoDatum.until(buchungsDatum, ChronoUnit.DAYS);
        int zeitSpanne2 = (int) stornoDatum.until(anreiseDatum, ChronoUnit.DAYS);
        if (zeitSpanne1 <= 7) {
            gesamtPreis *= 1;
        } else if (zeitSpanne2 >= 21) {
            gesamtPreis *= 0.5;
        } else if (zeitSpanne2 >= 14) {
            gesamtPreis *= 0.3;
        } else {
            gesamtPreis = 0;
        }
        return new StornoDTO(buchungCRUDRepository.findById(buchungId).get().getBuchungsId(), preisBerechnung(buchungId, hotelId).getGesamtPreis(), gesamtPreis, buchungsDatum, stornoDatum, anreiseDatum);
    }

    public void postBuchungsPerson(BuchungsPerson buchungsPerson) {
        buchungsPersonCRUDRepository.save(buchungsPerson);
    }


    public BuchungsDTO buchungsBerechnung(int buchungsPersonId) {

        List<Buchung> buchungsarray = new ArrayList<>(buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getBuchungSet());

        ZeitraumDTO zeitraumDTO = new ZeitraumDTO(buchungsarray.get(buchungsarray.size() - 1).getAnreiseDatum(),
                buchungsarray.get(buchungsarray.size() - 1).getAbreiseDatum());

        Set<BuchungsMitReisende> buchungsMitReisendeSet = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getBuchungsMitReisendeSet();

        List<Zimmer> gebuchteZimmer = new ArrayList<>();
        buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getBuchungSet().forEach(b -> b.getBuchungZimmerSet().forEach(z -> gebuchteZimmer.add(z.getZimmer())));
        String verpflegung = String.valueOf(buchungsarray.get(buchungsarray.size() - 1).getVerpflegung());
        List<Set<Extras>> extras = new ArrayList<>();
        buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getBuchungSet().forEach(b -> extras.add(b.getExtras()));

        Double preisDerBuchung = preisPerBuchung(buchungsarray.get(buchungsarray.size() - 1).getBuchungsId());

        return new BuchungsDTO(zeitraumDTO, buchungsPersonCRUDRepository.findById(buchungsPersonId).get(), buchungsMitReisendeSet, gebuchteZimmer,
                verpflegung, extras, ZeitraumDTO.zeitSpanneInTagen(zeitraumDTO.getVon(), zeitraumDTO.getBis()), preisDerBuchung);
    }

    public KompletteBuchungDTOPost kompletteBuchungUebersicht(int buchungsPersonId) {
        String vorName = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getVorname();
        String nachName = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getNachname();
        LocalDate geburtsDatum = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getGebDatum();
        String eMailAdresse = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getEMailAdresse();
        String wohnAdresse = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getWohnAdresse();
        String kredtKartenNummer = buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getKreditKarteNummer();

        Set<BuchungsMitreisendenDTOBuchung> buchungsMitreisendenDTOBuchungSet = new HashSet<>();
        buchungsPersonCRUDRepository.findById(buchungsPersonId).get().
                getBuchungsMitReisendeSet().forEach(bm -> buchungsMitreisendenDTOBuchungSet.add(new BuchungsMitreisendenDTOBuchung(bm.getVornameBMR(),
                        bm.getNachnameBMR(), bm.getAlterBMR())));

        List<Buchung> buchungsarray = new ArrayList<>(buchungsPersonCRUDRepository.findById(buchungsPersonId).get().getBuchungSet());

        List<Zimmer> gebuchteZimmer = new ArrayList<>();
        buchungsarray.get(buchungsarray.size() - 1).getBuchungZimmerSet().forEach(z -> gebuchteZimmer.add(z.getZimmer()));

        Set<Integer> zimmerDTOBuchungSet = new HashSet<>();
        gebuchteZimmer.forEach(zs -> zimmerDTOBuchungSet.add(zs.getId()));

        LocalDate anreiseDatum = ZeitraumDTO.convertToLocalDateViaInstant(buchungsarray.get(buchungsarray.size() - 1).getAnreiseDatum());
        LocalDate abreiseDatum = ZeitraumDTO.convertToLocalDateViaInstant(buchungsarray.get(buchungsarray.size() - 1).getAbreiseDatum());

        String verpflegung = String.valueOf(buchungsarray.get(buchungsarray.size() - 1).getVerpflegung());
        Set<String> extras = new HashSet<>();
        buchungsarray.get(buchungsarray.size() - 1).getExtras().forEach(e -> extras.add(e.getName()));

        return new KompletteBuchungDTOPost(vorName, nachName, geburtsDatum, eMailAdresse,
                wohnAdresse, kredtKartenNummer, buchungsMitreisendenDTOBuchungSet, zimmerDTOBuchungSet, anreiseDatum, abreiseDatum, verpflegung, extras);

    }

    public void postBuchung(KompletteBuchungDTOPost kompletteBuchungDTOPost) {
        BuchungsPerson buchungsPerson = new BuchungsPerson(kompletteBuchungDTOPost);

        Date anreiseDatum = Date.from(ZeitraumDTO.convertToDateViaInstant(kompletteBuchungDTOPost.getAnreiseDatum()));
        Date abreiseDatum = Date.from(ZeitraumDTO.convertToDateViaInstant(kompletteBuchungDTOPost.getAbreiseDatum()));
        buchungsPersonCRUDRepository.save(buchungsPerson);

        Set<BuchungsMitreisendenDTOBuchung> buchungsMitreisendenDTOBuchungSet = kompletteBuchungDTOPost.getBuchungsMitreisendenDTOBuchungSet();
        for (BuchungsMitreisendenDTOBuchung buchungsMitreisendenDTOBuchung : buchungsMitreisendenDTOBuchungSet) {
            BuchungsMitReisende buchungsMitReisende = new BuchungsMitReisende(buchungsMitreisendenDTOBuchung);
            buchungsPerson.addBuchungsMitReisendeSet(buchungsMitReisende);
            buchungsMitReisende.setBuchungsPerson(buchungsPerson);
            buchungsMitReisendeCRUDRepository.save(buchungsMitReisende);
        }
        Set<Zimmer> gebuchteZimmer = new HashSet<>();
        kompletteBuchungDTOPost.getZimmerIds().forEach(id -> gebuchteZimmer.add(zimmerCRUDRepository.findById(id).get()));


        Set<Extras> extras = new HashSet<>();
        kompletteBuchungDTOPost.getExtras().forEach(e -> extras.add(Extras.valueOf(e.toUpperCase())));

        Verpflegung verpflegung = Verpflegung.valueOf(kompletteBuchungDTOPost.getVerpflegung().toUpperCase());

        Buchung buchung = new Buchung(verpflegung, anreiseDatum, abreiseDatum, buchungsPerson, extras);


        buchungsPerson.addBuchungSet(buchung);
        buchung.setBuchungsPerson(buchungsPerson);

        buchungsPersonCRUDRepository.save(buchungsPerson);
        buchungCRUDRepository.save(buchung);

        int preisDerVerplegung = Verpflegung.valueOf(kompletteBuchungDTOPost.getVerpflegung().toUpperCase()).getPreisDerVerplegung();
        double gesamtVerplegung = buchungsMitreisendenDTOBuchungSet.stream().mapToDouble(e -> e.getPreisRabatt() * preisDerVerplegung).sum() + preisDerVerplegung;

        int zeitSpanne = ZeitraumDTO.zeitSpanneInTagen(anreiseDatum, abreiseDatum);


        double preisPerBuchung = preisErhoehung(buchung.getBuchungsId(), gebuchteZimmer.stream().findFirst().get().getHotel().getHotelId()) + zeitSpanne * gesamtVerplegung;


        gebuchteZimmer.forEach(z -> setZimmerBuchung(buchung.getBuchungsId(), z.getId()));
        buchung.setPreisBerechungBuchung(preisPerBuchung);
        buchungCRUDRepository.save(buchung);
    }
}

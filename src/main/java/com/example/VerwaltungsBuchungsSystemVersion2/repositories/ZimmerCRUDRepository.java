package com.example.VerwaltungsBuchungsSystemVersion2.repositories;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.Zimmer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZimmerCRUDRepository extends CrudRepository<Zimmer, Integer> {
}

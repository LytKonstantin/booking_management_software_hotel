package com.example.VerwaltungsBuchungsSystemVersion2.repositories;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.Hotel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelCRUDRepository extends CrudRepository<Hotel, Integer> {
}

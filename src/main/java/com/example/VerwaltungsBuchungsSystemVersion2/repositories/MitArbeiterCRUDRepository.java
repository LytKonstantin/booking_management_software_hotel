package com.example.VerwaltungsBuchungsSystemVersion2.repositories;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.MitArbeiter;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Abteilung;
import com.example.VerwaltungsBuchungsSystemVersion2.enums.Geschlecht;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MitArbeiterCRUDRepository extends CrudRepository<MitArbeiter, Integer> {
    List<MitArbeiter> findByAbteilungIs(Abteilung abteilung);

    List<MitArbeiter> findByGeschlechtIs(Geschlecht geschlecht);

    void findById(MitArbeiter mitArbeiter_id);
}

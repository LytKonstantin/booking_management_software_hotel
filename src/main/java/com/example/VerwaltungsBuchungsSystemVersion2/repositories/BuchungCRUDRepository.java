package com.example.VerwaltungsBuchungsSystemVersion2.repositories;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.Buchung;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuchungCRUDRepository extends CrudRepository<Buchung, Integer> {
}

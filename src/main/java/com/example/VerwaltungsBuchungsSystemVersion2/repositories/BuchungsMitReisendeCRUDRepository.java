package com.example.VerwaltungsBuchungsSystemVersion2.repositories;

import com.example.VerwaltungsBuchungsSystemVersion2.entities.BuchungsMitReisende;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuchungsMitReisendeCRUDRepository extends CrudRepository<BuchungsMitReisende, Integer> {
}
